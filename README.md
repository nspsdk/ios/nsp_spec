# NSP_Spec

## Requirements

---

This SDK works with an **APP ID** provided by [SmartProfile](https://www.smartp.com/c.dspHome/smartprofile/)

## Integrating the SDK

---
NSP_SDK is available through [CocoaPods](https://cocoapods.org). To install

it, simply add the following lines to your Podfile:

```ruby
source 'https://gitlab.com/nspsdk/ios/nsp_spec.git'

pod 'NSP_SDK'
```

## Initialize the SDK

---

In your **AppDelegate** :

```objective-c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions { 
 // Init Nsp SDK
 NspCore* core = [[NspCore alloc] initWithAppId:@"APP_ID" andGDPROption: /* GDPROptions */];
 
 [core setLogin:@"LOGIN"];
 [core startup];
}
```

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
 // Init Nsp SDK
 let core = NspCore(appId: "APP_ID", andGDPROption: /* GDPROptions */)
 
 core?.login = "LOGIN"
 core?.startup()
}
```

# GDPROptions values

---

- **Full** : All informations will be recorded.
- **Anon** : All informations will be anonymized before being recorded.
- **None** : No informations will be recorded.

## Push Notifications initial configuration

---

As a reminder, NSP need to have a valid certificate in order to communicate with Apple Push Notification Services _(APNS)_. The file recommended is :

- **.p8 files (recommended)** : Valid for all the apps added to your Apple developer account. You will need to specify the Application Identifier _(App ID)_ or the Bundle ID of your app as well.

### Step 1 : Downloading the .P8 File

Head to the [Apple Developer Member center](https://developer.apple.com/account/resources/authkeys/list), then go to the _"Keys"_ menu and add a new key.

![[p8_file.png]](img/p8_file.png)

Give a name to your key, tick the _"Apple Push Notifications Service"_ box and continue until you can download the .p8 file.

![[p8_file_2.png]](img/p8_file_2.png)

### Send the .P8 File

Once downloaded, **you must send us the .P8 File along those informations** :

- **Bundle ID**: We recommend you use the bundle ID you will find in Xcode.
- **Team ID**: The team ID is also available from the Developer Console [here](https://developer.apple.com/account/#/membership/). It is also shown under your name in the top bar of the place you created your p8 key.
- **Key ID**: The Key ID is in the default Key filename. You can also find the Key ID in the Apple Developer Member center, where you generated it.

## Setting up Push Notifications

---

First, select your project in Xcode sidebar, then click on the `Signing & Capabilities` tab.
If `Push Notifications` isn't already there, click on `+ Capability` and pick `Push Notifications`. If there is a `Fix` button shown, press it.

![[push_notif_xcode.png]](img/push_notif_xcode.png)

Add those lines in your **Cocoapod file** and do a **pod install**:

```ruby
pod 'FirebaseAuth'
pod 'FirebaseMessaging'
```

Then, you need to add a few lines to your app delegate in order to receive push notifications.

To ask for the permission to display notifications and register the current device for push, call the method `[application requestNotificationAuthorization]` in your application delegate.

```objective-c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
 // Init Nsp SDK
 NspCore* core = [[NspCore alloc] initWithAppId:@"APP_ID" andGDPROption: /* GDPROptions */];
 
 [core setLogin:@"LOGIN"];
 [core startup];
 
 // Prepare Options and initialize Notifications
 FIROptions* options = [[FIROptions alloc] initWithGoogleAppID: @"APP_ID" GCMSenderID: @"GCM_SENDER_ID"];
 [options setAPIKey: @"API_KEY"];
 [options setProjectID: @"PROJECT_ID"];
    
 [FIRApp configureWithOptions:options];
 [FIRMessaging messaging].delegate = self;
    
 // Request Notification authorization
 [UNUserNotificationCenter currentNotificationCenter].delegate = self;
 UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
   UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
 [[UNUserNotificationCenter currentNotificationCenter]
   requestAuthorizationWithOptions:authOptions
   completionHandler:^(BOOL granted, NSError * _Nullable error) {
     // ...
   }];
   
 // Register when needed to receive notifications
 [application registerForRemoteNotifications];
}

// Send device token to NSP API
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);

    [core setNotificationToken: fcmToken];
}
```

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
  // Init Nsp SDK
  let core = NspCore(appId: "APP_ID", andGDPROption: /* GDPROptions */)

  core?.login = "LOGIN"
  core?.startup()
 
  // Prepare Options and initialize Notifications
  let options = FirebaseOptions(googleAppID: "APP_ID", gcmSenderID: "GCM_SENDER_ID")
  options.apiKey = "API_KEY"
  options.projectID = "PROJECT_ID"

  FirebaseApp.configure(options: options)
  Messaging.messaging().delegate = self

  // Request Notification authorization
  UNUserNotificationCenter.current().delegate = self
  let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
  UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { granted, error in
    // ...
  }

  // Register when needed to receive notifications
  UIApplication.shared.registerForRemoteNotifications()
}

// Send device token to NSP API
func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
 guard let token = fcmToken else { return }
 print("FCM registration token: ", token)
 core?.setNotificationToken(token)
}
```

## Author

---

<devmobile@nsp-fr.com>

## License

---

NSP_SDK is available under the MIT license. See the LICENSE file for more info.
